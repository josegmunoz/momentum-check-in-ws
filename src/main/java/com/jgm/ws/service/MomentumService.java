package com.jgm.ws.service;

import java.util.Date;
import java.util.List;

import com.jgm.ws.model.MomentumBean;

public interface MomentumService {
	
	void save(MomentumBean m);

	String getHoursPerMonth(String name, long month);
	
	String getHoursPersonPerPeriod(String name, Date startDate, Date endDate);
	
	List<MomentumBean> getAll();

	List<MomentumBean> getEntriesPerMonth(long month);

	List<MomentumBean> getEntriesCustomPeriod(Date startDate, Date endDate);	

}
