package com.jgm.ws.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class MomentumBean {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long momentumId;
	private String name;
	private Time dailyHours;
	private Date clockIn;
	private Date clockOut;
	
	public long getMomentumId() {
		return momentumId;
	}
	public void setMomentumId(long momentumId) {
		this.momentumId = momentumId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@JsonFormat(pattern="HH:mm:ss")
	public Time getDailyHours() {
		return dailyHours;
	}
	public void setDailyHours(Time dailyHours) {
		this.dailyHours = dailyHours;
	}
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getClockIn() {
		return clockIn;
	}
	public void setClockIn(Date clockIn) {
		this.clockIn = clockIn;
	}
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getClockOut() {
		return clockOut;
	}
	public void setClockOut(Date clockOut) {
		this.clockOut = clockOut;
	}
	
	public MomentumBean() {
		
	}
	
	
}

