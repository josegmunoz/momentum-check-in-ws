package com.jgm.ws.model;

import java.util.Date;

public class ErrorMessage {
	private Date timestamp;
	private String message;
	public ErrorMessage(String message, Date timestamp) {
		
		this.timestamp = timestamp;
		this.message = message;
	}
	public ErrorMessage() {
		
	
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date gettimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
}
