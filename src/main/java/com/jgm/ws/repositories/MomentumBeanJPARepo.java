package com.jgm.ws.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.jgm.ws.model.MomentumBean;



public interface MomentumBeanJPARepo extends JpaRepository<MomentumBean,Long> {

	@Query(value = 
			"select concat(SEC_TO_TIME(TIME_TO_SEC(sum(hours_day))),'') from registros where name=?1 and MONTH(clock_in) = ?2",
			nativeQuery = true)
			String getHoursPerMonth(String name,long month);
	@Query(value = 
			"SELECT concat(SEC_TO_TIME(SUM(TIME_TO_SEC(hours_day))),'') FROM registros WHERE clockIn BETWEEN ?2 AND ?3 and name=?1",
			nativeQuery = true)
			String getHoursPersonPerPeriod(String name,Date startDate,Date endDate);
	
	@Query(value = 
			"SELECT * FROM registerperson WHERE MONTH(clock_in) = ?1",
			nativeQuery = true)
	List<MomentumBean> findByMonth(long month);
	
	
	@Query(value = 
			"SELECT * FROM registerperson WHERE clock_in BETWEEN ?1 AND ?2",
			nativeQuery = true)
	List<MomentumBean> getRecordsPerPeriod(Date startDate, Date endDate);

	
}
