package com.jgm.ws.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jgm.ws.model.ExcelBean;
import com.jgm.ws.model.MomentumBean;
import com.jgm.ws.service.MomentumService;

@Controller
public class ExcelController {
	@Autowired
	MomentumService momentumService;
	
	@RequestMapping(value = "/read")
	public ModelAndView HandleUpload() throws IOException {
		// reading excel
		InputStream ExcelFileToRead = null;
		Workbook workbook = null;
		String filePath = "C:\\Users\\joseg.munoz\\Desktop\\Registros Momentum.xls";
		try {
			ExcelFileToRead = new FileInputStream(filePath);
			workbook = new HSSFWorkbook(ExcelFileToRead);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Sheet sheet = workbook.getSheetAt(0);
		DataFormatter dataFormatter = new DataFormatter();
		Row row;
		Cell cell;
		Iterator rows = sheet.rowIterator();
		List<ExcelBean> momentumList = new ArrayList<>();
		rows.next();
		while (rows.hasNext()) {
			row = (Row) rows.next();
			Iterator cells = row.cellIterator();
			ExcelBean registroExcel = new ExcelBean();
			int i = 0;
			//Num, Department, Name, ID, Date, Verify, ClockIn, ClockOut, DeviceID, DeviceName, UserExtFmt2
			while (cells.hasNext()) {
				cell = (Cell) cells.next();
				String cellValue = dataFormatter.formatCellValue(cell);
				switch (i) {
				case 0:
					registroExcel.setNum(Integer.parseInt(cellValue));
					break;
				case 2:
					registroExcel.setName(cellValue);
					break;
				case 3:
					registroExcel.setId(Integer.parseInt(cellValue));
					break;
				case 4:
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date d1;
					try {
						d1 = format.parse(cellValue);
						registroExcel.setDate(d1);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					break;
				case 6:
					registroExcel.setClockType(cellValue);
					break;
				case 7:
					registroExcel.setDeviceId(Integer.parseInt(cellValue));
					break;
				}
				momentumList.add(registroExcel);
				i++;
			}
		}
		ExcelFileToRead.close();

		List<String> uniqueNames = 
				momentumList.stream()
				.map(ExcelBean::getName)
				.distinct()
				.collect(Collectors.toList());
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		
		HashMap<String, List<String>> map = new HashMap<String, List<String>>();
		for (String name : uniqueNames) {
			List<String> dateClock =  
					momentumList.stream()
					.filter(r -> r.getName() == name)
					.map(f -> {
						return dateFormat.format(f.getDate());
					})
					.distinct()
					.collect(Collectors.toList());
			map.put(name, dateClock);
		}
		
		
		map.forEach((k, v) -> {
			System.out.println("key: " + k);
			v.forEach(System.out::println);
		});
		
		
		map.forEach((k, v) -> {
			v.stream()
			.forEach(d -> {
				List<Date> checkins = 
						momentumList.stream()
						.filter(o -> o.getName().equals(k))//name filter
						.filter(o -> dateFormat.format(o.getDate()).equals(d)).distinct().map(ExcelBean::getDate)//date filter
						.collect(Collectors.toList());

				System.out.println(k + "*******Day Checkins for: " + checkins.size());

				if(checkins.size()>1) {
					long diff = Collections.max(checkins).getTime() - Collections.min(checkins).getTime();
					long diffSeconds = diff / 1000 % 60;
					long diffMinutes = diff / (60 * 1000) % 60;
					long diffHours = diff / (60 * 60 * 1000) % 24;
					long diffDays = diff / (24 * 60 * 60 * 1000);
					
					
					MomentumBean registry =new MomentumBean();
					registry.setName(k);
					registry.setClockIn(Collections.min(checkins));
					registry.setClockOut(Collections.max(checkins));
					registry.setDailyHours(new Time(diff));
					MomentumService.save(registry);
					
					
				
					checkins.stream().forEach(System.out::println);
					System.out.println(
							k + " -- the amount of hours for this day are: " + diffHours + ":" + diffMinutes + ":" + diffSeconds);
				}
			});
		});
		//Ends Parseing 

		return null;
	}
}
