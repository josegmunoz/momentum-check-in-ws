package com.jgm.ws.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jgm.ws.model.ErrorMessage;
import com.jgm.ws.model.MomentumBean;
import com.jgm.ws.service.MomentumService;


@RestController
@RequestMapping("api/v1")
public class MomentumController {
	
	@Autowired
	MomentumService momentumService;
	
	@GetMapping(value = "momentums")
	public Object momentums() {
		List<MomentumBean> list = momentumService.getAll();
		if (list.isEmpty())
			return new ErrorMessage("Empty List", new Date());
		else
			return list;
	}
	
	//GET: Obtain hours per month by name
	@GetMapping(value = "momentum/{name}/{month}")
	public Object gethoursPerMount(@PathVariable String name, @PathVariable long month) {
		String hours = momentumService.getHoursPerMonth(name, month);
		if (hours == null)
			return new ErrorMessage("No existe registro", new Date());
		else
			return hours;
	}

	//POST: Sends name, startDate & EndDate to obtain user stats based on dates
	@PostMapping("momentum/")
	public Object getHoursPersonPerPeriod(@RequestParam("name") String name,
			@RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
			@RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {
		String hours = momentumService.getHoursPersonPerPeriod(name, startDate, endDate);
		if (hours == null)
			return new ErrorMessage("Emtpy List, Record does not exist", new Date());
		else
			return hours;
	}

	//GET: Obtains all registries from a user in the given month
	@GetMapping(value = "period/{month}")
	public Object getPersonsRecords(@PathVariable long month) {
		List<MomentumBean> list = momentumService.getEntriesPerMonth(month);
		if (list.isEmpty())
			return new ErrorMessage("Empty List", new Date());
		else
			return list;
	}

	//POST:Envia el parametron de startDate y Enddate para obtener todos los usuarios limitados por la fecha de inicio y fin. 
	
	@PostMapping("period/")
	public Object getRecordsPerPeriod(@RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
			@RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {

		List<MomentumBean> list = momentumService.getEntriesCustomPeriod(startDate, endDate);
		if (list.isEmpty())
			return new ErrorMessage("Empty List, Record does not exist", new Date());
		else
			return list;
	}
}
